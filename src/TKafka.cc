/**
 **********************************************
 *
 * \file TKafka.cc
 * \brief Source code of the TKafka class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <TKafka.h>
#include <RKafka/TKafkaImpl.h>

const char *TKafka::DEFAULT_HOST  = "localhost";
const int   TKafka::DEFAULT_PORT  = 9092;
const int   TKafka::DEFAULT_DELAY = 0;
const int   TKafka::DEFAULT_TICK  = 100;

volatile sig_atomic_t TKafka::bInterrupt = false;
volatile sig_atomic_t TKafka::bCallback  = false;

const int TKafka::DefaultPartition = RD_KAFKA_PARTITION_UA;

TKafka::TKafka(TString server, int port,    TString topics,              TString groupid): TKafka::TKafka(std::vector<TString>({server+":"+TString::Itoa(port, 10)}), ROOT::IOPlus::Helpers::Explode(" ", topics), groupid) {}
TKafka::TKafka(std::vector<TString> server, std::vector<TString> topics, TString groupid): TKafka::TKafka(TKafka::Impl::ParseBroker(server), topics, groupid) {}

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

TKafka::~TKafka()
{
        if(this->pImpl) {
         
                delete this->pImpl;
                this->pImpl = NULL;
        }
}

TKafka::TKafka(std::vector<Broker> brokers, std::vector<TString> topics, TString groupid): brokers(brokers), topics(topics), groupid(groupid.EqualTo("") ? ROOT::IOPlus::Helpers::GetRandomStr(6) : groupid)
{
        this->max_transactions = -1;
        this->transactions = 0;

        this->tick = TKafka::DEFAULT_DELAY;
        this->delay = 0;

        this->partition = RD_KAFKA_PARTITION_UA;
        this->offset = -1;
        this->offsetShiftBy = 0;
        this->offsetForTimes = -1;
        this->bOffsetEarliest = false;
        this->bOffsetLatest = false;

        this->bRepeat = false;
        this->pImpl = new TKafka::Impl();
};

std::time_t TKafka::Time(const std::string& str, bool is_dst, const std::string& format)
{
        std::tm t{};
        t.tm_isdst = is_dst ? 1 : 0;
        std::stringstream ss(str);
        ss >> std::get_time(&t, format.c_str());
        return mktime(&t);
}

TString TKafka::__toString() {

        TString brokers = "";
        for(int i = 0, N = this->brokers.size(); i < N; i++) {

                TString host = this->brokers[i].host;
                        host = host.EqualTo("") ? TString(DEFAULT_HOST) : host;

                int port = this->brokers[i].port;
                    port = !port ? DEFAULT_PORT : port;

                brokers = (TString) brokers + " " + host.Data() + ":" + TString::Itoa(port,10);
        }

        return brokers.Strip(TString::kBoth, ' ');
}

void TKafka::Print(Option_t *option)
{
        if(bRun) TPrint::Info(__METHOD_NAME__, "Connection is established.");
        else TPrint::Error(__METHOD_NAME__, "Failed to connect");

        TPrint::Message(__METHOD_NAME__, "Brokers registered: %s", this->__toString().Data());
        TPrint::Message(__METHOD_NAME__, "Subscribed to %d topic(s)", this->GetNSubscribers());
}

bool TKafka::Connect(Mode mode, Dict dict)
{
        bRun = false;
        char errstr[512];        /* librdkafka API error reporting buffer */

        // Prepare brokers
        TString brokers = this->__toString();

        // Initialize configuration
        if(this->pImpl->GetConfiguration() != NULL) {

                this->Disconnect();
                this->pImpl->SetConfiguration(NULL);
        }

        this->pImpl->SetConfiguration(rd_kafka_conf_new());
        if(this->pImpl->GetConfiguration() == NULL) {
                TPrint::Error(__METHOD_NAME__, "Failed to initialize kafka configuration container.");
                return bRun;
        }

        DictIter it;
        for (it = dict.begin(); it != dict.end(); it++)
        {
                if (rd_kafka_conf_set(this->pImpl->GetConfiguration(), it->first.Data(), it->second.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                        TPrint::Error(__METHOD_NAME__, "Failed to set `" + it->first + "` to '" + it->second + "'\n%s", &errstr[0]);
                        this->pImpl->SetConfiguration(NULL);
                        return bRun;
                }                
        }

        /* Set bootstrap broker(s) as a comma-separated list of
        * host or host:port (default port 9092).
        * librdkafka will use the bootstrap brokers to acquire the full
        * set of brokers from the cluster. */
        if(dict.find("bootstrap.servers") != dict.end()) {
                TPrint::Error(__METHOD_NAME__, "Unexpected option `bootstrap.servers` provided through the optional Dict in " + __METHOD_NAME__ + "\nPlease pass `bootstrap.servers` through " + __METHOD_NAME__ + "::" + __METHOD_NAME__ + " instead.");
                return bRun;
        }

        if (rd_kafka_conf_set(this->pImpl->GetConfiguration(), "bootstrap.servers", brokers.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                TPrint::Error(__METHOD_NAME__, "Failed to reach bootstrap servers: %s\n%s", brokers.Data(), &errstr[0]);
                this->pImpl->SetConfiguration(NULL);
                return bRun;
        }

        if(this->bLowLatency) {

                if(dict.find("linger.ms")!=dict.end()) {
                        TPrint::Error(__METHOD_NAME__, "Unexpected option `linger.ms` provided through the optional Dict in " + __METHOD_NAME__ + "\n(or disable low-latency option instead)", groupid.Data(), &errstr[0]);
                        return bRun;
                }

                if (rd_kafka_conf_set(this->pImpl->GetConfiguration(), "linger.ms", "0.1", &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                        TPrint::Error(__METHOD_NAME__, "Failed to set `linger.ms` to '0.1'\n%s", &errstr[0]);
                        this->pImpl->SetConfiguration(NULL);
                        return bRun;
                }
        }

        rd_kafka_message_t *rkm;
        switch(mode) {

                case TKafka::Mode::Producer:

                        /* Set the delivery report callback.
                        * This callback will be called once per message to inform
                        * the application if delivery succeeded or failed.
                        * See dr_msg_cb() above.
                        * The callback is only triggered from rd_kafka_poll() and
                        * rd_kafka_flush(). */
                        rd_kafka_conf_set_dr_msg_cb(this->pImpl->GetConfiguration(), TKafka::Impl::_dr_msg_cb);

                        this->pImpl->SetInstance(rd_kafka_new(RD_KAFKA_PRODUCER, this->pImpl->GetConfiguration(), errstr, sizeof(errstr)));
                        if (!this->pImpl->GetInstance()) {

                                TPrint::Error(__METHOD_NAME__, "Failed to create new producer.\n%s", &errstr[0]);
                                return bRun;
                        }

                        break;

                default:
                case TKafka::Mode::Consumer:

                        /* Set the consumer group id.
                        * All consumers sharing the same group id will join the same
                        * group, and the subscribed topic' partitions will be assigned
                        * according to the partition.assignment.strategy
                        * (consumer config property) to the consumers in the group. */
                        if(dict.find("group.id") != dict.end()) {
                                TPrint::Error(__METHOD_NAME__, "Unexpected option `group.id` provided through the optional Dict in " + __METHOD_NAME__ + "\nPlease pass `bootstrap.servers` through " + __METHOD_NAME__ + "::" + __METHOD_NAME__ + " instead.");
                                return bRun;
                        }
                        
                        if (rd_kafka_conf_set(this->pImpl->GetConfiguration(), "group.id", groupid.Data(), &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                                TPrint::Error(__METHOD_NAME__, "Failed to set \"%s\" group ID.\n%s", groupid.Data(), &errstr[0]);
                                this->pImpl->SetConfiguration(NULL);
                                return bRun;
                        }

                        /* If there is no previously committed offset for a partition
                        * the auto.offset.reset strategy will be used to decide where
                        * in the partition to start fetching messages.
                        * By setting this to earliest the consumer will read all messages
                        * in the partition if there was no previously committed offset. */                           
                        if(dict.find("auto.offset.reset") == dict.end()) {

                                if (this->bOffsetLatest) {
                                        
                                        if(rd_kafka_conf_set(this->pImpl->GetConfiguration(), "auto.offset.reset", "latest", &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                                                TPrint::Error(__METHOD_NAME__, "Failed to set offset.\n%s", &errstr[0]);
                                                this->pImpl->SetConfiguration(NULL);
                                                return bRun;
                                        }
                                        
                                } else if (this->bOffsetEarliest) {
                                        
                                        if (rd_kafka_conf_set(this->pImpl->GetConfiguration(), "auto.offset.reset", "earliest", &errstr[0], sizeof(errstr)) != RD_KAFKA_CONF_OK) {
                                                TPrint::Error(__METHOD_NAME__, "Failed to set offset.\n%s", &errstr[0]);
                                                this->pImpl->SetConfiguration(NULL);
                                                return bRun;
                                        }
                                }
                        }

                        this->pImpl->SetInstance(rd_kafka_new(RD_KAFKA_CONSUMER, this->pImpl->GetConfiguration(), errstr, sizeof(errstr)));
                        if (!this->pImpl->GetInstance()) {

                                TPrint::Error(__METHOD_NAME__, "Failed to create new consumer.\n%s", &errstr[0]);
                                return bRun;
                        }

                        //
                        // Configure offsets
                        // for (int i = 0; i < topics.size(); i++) {}
                        // rd_kafka_topic_partition_list_t* partition_list = rd_kafka_topic_partition_list_new(1);
                        // rd_kafka_topic_partition_t     * pt0            = rd_kafka_topic_partition_list_add(partition_list, topic, 0);
                        //                                  pt0->offset    = ~0; // set to max integer value

                        // rd_kafka_resp_err_t offsets_err = rd_kafka_offsets_for_times(this->pImpl->GetInstance(), partition_list, 10000);
                        // if (offsets_err != RD_KAFKA_RESP_ERR_NO_ERROR) printf("ERROR: Failed to get offsets: %d: %s.\n", offsets_err, rd_kafka_err2str(offsets_err));
                        // else printf("Successfully got high watermark offset %d.\n", pt0->offset);

                        this->pImpl->SetConfiguration(NULL); /* Configuration object is now owned, and freed,
                                      * by the rd_kafka_t instance. */

                        /* Redirect all messages from per-partition queues to
                         * the main queue so that messages can be consumed with one
                         * call from all assigned partitions.
                         *
                         * The alternative is to poll the main queue (for events)
                         * and each partition queue separately, which requires setting
                         * up a rebalance callback and keeping track of the assignment:
                         * but that is more complex and typically not recommended. */
                        rd_kafka_poll_set_consumer(this->pImpl->GetInstance());

                        rkm = rd_kafka_consumer_poll(this->pImpl->GetInstance(), this->tick);

                        /* Convert the list of topics to a format suitable for librdkafka */
                        this->pImpl->SetSubscription(rd_kafka_topic_partition_list_new(topics.size()));
                        for (int i = 0; i < topics.size(); i++) {

                                if(this->offset >= 0) rd_kafka_topic_partition_list_set_offset(this->pImpl->GetSubscription(), topics[i], this->partition, this->offset);
                                rd_kafka_topic_partition_list_add(this->pImpl->GetSubscription(), topics[i], this->partition);
                        }

                        rd_kafka_resp_err_t err = rd_kafka_subscribe(this->pImpl->GetInstance(), this->pImpl->GetSubscription());
                        if (err) {

                                TPrint::Error(__METHOD_NAME__, "Failed to subscribe to %d topics: %s", this->pImpl->GetSubscription()->cnt, rd_kafka_err2str(err));
                                rd_kafka_topic_partition_list_destroy(this->pImpl->GetSubscription());
                                rd_kafka_destroy(this->pImpl->GetInstance());
                                return bRun;
                        }

                        TPrint::Debug(1, __METHOD_NAME__, "Subscribed to %d topic(s)", this->GetNSubscribers());
                        TPrint::Debug(1, __METHOD_NAME__, "Waiting for rebalance and messages...", this->pImpl->GetSubscription()->cnt);

                        rd_kafka_topic_partition_list_destroy(this->pImpl->GetSubscription());

                        break;
        }

        /* Signal handler for clean shutdown */
        bRun = true;
        TKafka::EnableSignalHandler();

        time(&oot);
        return bRun;
}

int TKafka::GetNSubscribers() {
        return this->pImpl->GetSubscription()->cnt;
}

void TKafka::EnableSignalHandler(){

        TKafka::bInterrupt = false;

        TPrint::Debug(10, __METHOD_NAME__, "Enable Ctrl+C catcher..");
        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = TKafka::HandlerCTRL_C;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;
        sigaction(SIGINT, &sigIntHandler, NULL);
}

void TKafka::DisableSignalHandler() {

        TPrint::Debug(10, __METHOD_NAME__, "Disable Ctrl+C catcher..");
        signal(SIGINT, SIG_DFL);
}

void TKafka::HandlerCTRL_C(int s){

        std::cerr << std::endl;
        std::cerr << "** Caught signal " << s << std::endl;
        std::cerr << "** You stopped processing pressing CTRL-C.."<< std::endl;
        
        TKafka::bInterrupt = true;
}

int TKafka::Disconnect()
{
        TKafka::DisableSignalHandler();

        delete pImpl;
        pImpl = NULL;

        if(this->bRun) TPrint::Message(__METHOD_NAME__, "Connection closed.\n");
        this->bRun = false;
        
        return 1;
}


bool TKafka::Produce(TString topic, std::vector<unsigned char> message)
{
        int iTry = 0;

        TPrint::DecrementTab();

        bool bInteractive = message.size() == 0;
        if(bInteractive) TPrint::Message(__METHOD_NAME__,
        (TString)"Type some text and hit enter to produce message\n"+
                 "Or just hit enter to only serve delivery reports\n"+
                 "Press Ctrl-C or Ctrl-D to exit");

        while (this->bRun && (this->max_transactions < 0 || this->max_transactions > this->transactions)) {

                auto start = std::chrono::high_resolution_clock::now();
                if(bInterrupt) break;

                if(bInteractive) {

                        std::cout << "> ";

                        TString buffer;
                                buffer.ReadLine(std::cin);

                        message = str2vec(buffer);
                        this->transactions++;
                }

                size_t len = message.size();
                rd_kafka_resp_err_t err;

                if (len == 0) {
                        /* Empty line: only serve delivery reports */
                        rd_kafka_poll(this->pImpl->GetInstance(), 0 /*non-blocking */);
                        break;
                }

                if (message[len - 1] == '\n') /* Remove newline */
                        message[--len] = '\0';


                /*
                * Send/Produce message.
                * This is an asynchronous call, on success it will only
                * enqueue the message on the internal producer queue.
                * The actual delivery attempts to the broker are handled
                * by background threads.
                * The previously registered delivery report callback
                * (dr_msg_cb) is used to signal back to the application
                * when the message has been delivered (or failed).
                */
        retry:
                err = rd_kafka_producev(
                /* Producer handle */
                this->pImpl->GetInstance(),
                RD_KAFKA_V_TOPIC(topic.Data()),
                RD_KAFKA_V_PARTITION(partition),
                RD_KAFKA_V_MSGFLAGS(RD_KAFKA_MSG_F_COPY),
                RD_KAFKA_V_VALUE((void*) &message[0], len),
                RD_KAFKA_V_OPAQUE(NULL),
                RD_KAFKA_V_END);

                if (TPrint::ErrorIf(err, __METHOD_NAME__, "Failed to produce to topic %s: %s", topic.Data(), rd_kafka_err2str(err))) {

                        if (err == RD_KAFKA_RESP_ERR__QUEUE_FULL) {
                                /* If the internal queue is full, wait for
                                * messages to be delivered and then retry.
                                * The internal queue represents both
                                * messages to be sent and messages that have
                                * been sent or failed, awaiting their
                                * delivery report callback to be called.
                                *
                                * The internal queue is limited by the
                                * configuration property
                                * queue.buffering.max.messages */
                                rd_kafka_poll(this->pImpl->GetInstance(), 10*this->tick /*block for max X ms*/);

                                if(this->GetMaxTry() > 0 && this->GetMaxTry() > iTry++) break;

                                goto retry;
                        }

                } else TPrint::Info(__METHOD_NAME__, "Enqueued message (%zd bytes) for topic %s", TMath::Max((int) 0, (int) message.size()-1), topic.Data());

                /* A producer application should continually serve
                * the delivery report queue by calling rd_kafka_poll()
                * at frequent intervals.
                * Either put the poll call in your main loop, or in a
                * dedicated thread, or call it after every
                * rd_kafka_produce() call.
                * Just make sure that rd_kafka_poll() is still called
                * during periods where you are not producing any messages
                * to make sure previously produced messages have their
                * delivery report callback served (and any other callbacks
                * you register). */
                rd_kafka_poll(this->pImpl->GetInstance(), 0 /*non-blocking*/);
                if(!bInteractive) {

                        while(bRun && TKafka::bCallback == false)
                                rd_kafka_poll(this->pImpl->GetInstance(), this->tick);

                        if(!bRepeat) return 0;
                }

                auto elapsed = std::chrono::high_resolution_clock::now() - start;
                long long milliseconds = this->delay - std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                this->Await(milliseconds);
        }

        /* Wait for final messages to be delivered or fail.
         * rd_kafka_flush() is an abstraction over rd_kafka_poll() which
         * waits for all messages to be delivered. */
        TPrint::Warning(__METHOD_NAME__, "Flushing final messages..");
        rd_kafka_flush(this->pImpl->GetInstance(), 100 * this->tick /* wait for max 10 seconds */);

        /* If the output queue is still not empty there is an issue
         * with producing messages to the clusters. */
        TPrint::WarningIf(rd_kafka_outq_len(this->pImpl->GetInstance()) > 0, __METHOD_NAME__, "%d message(s) were not delivered", rd_kafka_outq_len(this->pImpl->GetInstance()));

        return 1;
}

void TKafka::Await(long long ms) {

        if(ms > 0) {

                usleep(1e3 * (ms % 1000));
                for(int i = 0, N = ms / 1000; i < N; i++) {
                        TPrint::Debug(1,__METHOD_NAME__, "Sleep delay before processing next transaction: %dms", 1000*(N-i));
                        usleep(1e6);
                }
        }
}

std::vector<unsigned char> TKafka::ReadFile(const char* fname)
{
    // open the file:
    std::ifstream file(fname, std::ios::binary);

    if(TPrint::ErrorIf(!file.is_open(), __METHOD_NAME__, "Failed to open file \"%s\"", fname))
        return {};

    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);

    // get its size:
    std::streampos fileSize;

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // reserve capacity
    std::vector<unsigned char>  vec;
                                vec.reserve(fileSize);

                                // read the data:
                                vec.insert(vec.begin(),
                                        std::istream_iterator<unsigned char>(file),
                                        std::istream_iterator<unsigned char>());

    return vec;
}

bool TKafka::HasPattern(TString fqfn)
{
        return fqfn.Index(TRegexp("{[0-9]*}")) != -1;
}

TString TKafka::SubstituteIdentifier(TString pattern, TString identifier)
{
        std::vector<TString> vIdentifier;
                        vIdentifier.push_back(identifier);

        return SubstituteIdentifier(pattern, vIdentifier);
}

TString TKafka::SubstituteIdentifier(TString pattern, std::vector<TString> identifier)
{
        if( TPrint::WarningIf(pattern.EqualTo(""), __METHOD_NAME__, "String used for substitution is empty..") ) return "";
        if( TPrint::WarningIf(identifier.size() == 0, __METHOD_NAME__, "Identifier vector is empty..") ) return "";

        TString globalIdentifier = identifier.size() > 0 ? identifier[0] : "";
        for( int i = 1, N = identifier.size(); i < N; i++)
                globalIdentifier += TKafka::kSeparator + identifier[i];

        TString filename = pattern;
                filename.ReplaceAll("{}", globalIdentifier);

        for( int i = 0, N = identifier.size(); i < N; i++)
                filename.ReplaceAll("{"+TString::Itoa(i,10)+"}", identifier[i]);

        return filename;
}

bool TKafka::Save(std::vector<unsigned char> bytes, TString fOutputPattern, std::vector<TString> fIdentifiers)
{
        if(TKafka::HasPattern(fOutputPattern) && !fIdentifiers.size()) {

		TPrint::Error(__METHOD_NAME__, "Save output enabled. Replacement char {0..x} found.. But no ID found..");
		return 0;

	} else if(fOutputPattern.EqualTo("")) {

		TString eMessage = "No output filename found.";
		TPrint::Debug(__METHOD_NAME__, eMessage);
                return 0;

	}

        TString fOutputFullname = TKafka::SubstituteIdentifier(fOutputPattern, fIdentifiers);
	TPrint::Message(__METHOD_NAME__, "Saving %d bytes into:\n%s", bytes.size(), fOutputFullname.Data());

        std::ofstream f;
                      f.open(fOutputFullname.Data(), std::ofstream::out | std::ofstream::app);

        if(f.is_open()) {

                std::copy(bytes.cbegin(), bytes.cend(), std::ostream_iterator<unsigned char>(f));
                f.close();

                return 1;
        }

        return 0;
}

std::vector<TKafka::Record> TKafka::Consume(double timeout, bool verbose)
{
        int iTry = 0;
        // if(TPrint::ErrorIf(this->output.Data(), __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};

        if(TPrint::ErrorIf(this->offset >= 0 && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};
        else if(TPrint::ErrorIf(this->offset > 0 && this->bOffsetEarliest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the earliest offset.")) return {};
        else if(TPrint::ErrorIf(this->bOffsetEarliest && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both earliest and latest offsets.")) return {};

        this->ResetTransactions();                

        auto first = std::chrono::high_resolution_clock::now();
        for (int i = 0; this->bRun && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - first).count() < timeout; i++) {

                auto start = std::chrono::high_resolution_clock::now();
                if(TKafka::bInterrupt) break;
                
                rd_kafka_message_t *rkm;

                rkm = rd_kafka_consumer_poll(this->pImpl->GetInstance(), this->tick);
                if (!rkm) {

                        if(TPrint::OnceEvery(10000, __FILE__, __LINE__))
                                TPrint::Debug(10, __METHOD_NAME__, "Waiting for the next incoming message..");

                        continue; /* Timeout: no message within dt,
                                   *  try again. This short timeout allows
                                   *  checking for `run` at frequent intervals.
                                   */
                }
                
                /* consumer_poll() will return either a proper message
                 * or a consumer error (rkm->err is set). */
                if (rkm->err) {
                        /* Consumer errors are generally to be considered
                         * informational as the consumer will automatically
                         * try to recover from all types of errors. */
                        TPrint::Warning(__METHOD_NAME__, "Consumer error: %s", rd_kafka_message_errstr(rkm));
                        rd_kafka_message_destroy(rkm);

                        if(this->GetMaxTry() > 0 && this->GetMaxTry() > iTry++) break;
                        continue;
                }

                /* Print the message key. */
                TString str = "";
                if (rkm->key)
                        str += TString::Format("[Length: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->key_len, this->groupid.Data(), rkm->offset);
                if (rkm->key && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->key, rkm->key_len))
                        str += TString::Format("Key: \"%.*s\"", (int)rkm->key_len, (const char *)rkm->key);

                /* Print the message value/payload. */
                if (rkm->payload) this->transactions++;
                if (rkm->payload)
                        str += TString::Format("[Payload: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->len, this->groupid.Data(), rkm->offset);

                /* Processing message. */
                TPrint::Debug(10, __METHOD_NAME__, "Message received in topic \"%s\" (offset %" PRId64") on partition: %" PRId32, rd_kafka_topic_name(rkm->rkt),  rkm->offset, rkm->partition, str.Data());
                
                TKafka::Record record;
                               record.offset = rkm->offset;
                               record.partition = rkm->partition;
                               record.topic = rd_kafka_topic_name(rkm->rkt);
                               record.epoch = time(0);

                if(rkm->key_len > 0) record.key = TString((const char *)rkm->key, rkm->key_len);
                if(rkm->len     > 0) record.msg = TString((const char *)rkm->payload, rkm->len);

                records.push_back(record);

                if(!this->output.EqualTo("")) {

                        if (rkm->payload && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->payload, rkm->len))
                                TPrint::Debug(10, __METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                
                        std::vector<unsigned char> bytes;
                                                   bytes.insert(bytes.end(), (const char *) rkm->payload,  (const char *) rkm->payload + rkm->len);

                        std::vector<TString> fIdentifiers;
                                             fIdentifiers.push_back(rd_kafka_topic_name(rkm->rkt));
                                             fIdentifiers.push_back(TString::Itoa(rkm->partition, 10));
                                             fIdentifiers.push_back(TString::Itoa(rkm->offset, 10));

                        TKafka::Save(bytes, this->output, fIdentifiers);

                } else if(verbose) {

                        if (rkm->payload && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->payload, rkm->len))
                                TPrint::Message(__METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                }

                rd_kafka_message_destroy(rkm);

                auto elapsed = std::chrono::high_resolution_clock::now() - start;
                long long milliseconds = this->delay - std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                
                this->Await(milliseconds);
        }

        return records;
}

std::vector<TKafka::Record> TKafka::ConsumeN(int N, bool verbose, bool bProgressBar)
{
        int iTry = 0;
        // if(TPrint::ErrorIf(this->output.Data(), __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};

        if(TPrint::ErrorIf(this->offset >= 0 && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the latest offset.")) return {};
        else if(TPrint::ErrorIf(this->offset > 0 && this->bOffsetEarliest, __METHOD_NAME__, "Ambiguous options provided. You asked for both a specific offset and the earliest offset.")) return {};
        else if(TPrint::ErrorIf(this->bOffsetEarliest && this->bOffsetLatest, __METHOD_NAME__, "Ambiguous options provided. You asked for both earliest and latest offsets.")) return {};

        this->ResetTransactions();                
        for (int i = 0; this->bRun && (this->max_transactions < 0 || this->max_transactions > this->transactions) && (N == 0 || this->transactions < N); i++) {

                auto start = std::chrono::high_resolution_clock::now();
                if(TKafka::bInterrupt) break;
                
                rd_kafka_message_t *rkm;

                rkm = rd_kafka_consumer_poll(this->pImpl->GetInstance(), this->tick);
                if (!rkm) {

                        if(TPrint::OnceEvery(10000, __FILE__, __LINE__))
                                TPrint::Debug(10, __METHOD_NAME__, "Waiting for the next incoming message..");
 
                        continue; /* Timeout: no message within dt,
                                   *  try again. This short timeout allows
                                   *  checking for `run` at frequent intervals.
                                   */
                }

                if(bProgressBar && (N > 0 || this->max_transactions > 0)) gPrint->ProgressBar(__METHOD_NAME__, "", this->transactions, TMath::Max(N, this->max_transactions));
                if(TPrint::IsDebugEnabled(10)) {
                        TPrint::ErasePreviousLine();
                }
                
                /* consumer_poll() will return either a proper message
                 * or a consumer error (rkm->err is set). */
                if (rkm->err) {
                        /* Consumer errors are generally to be considered
                         * informational as the consumer will automatically
                         * try to recover from all types of errors. */
                        TPrint::Warning(__METHOD_NAME__, "Consumer error: %s", rd_kafka_message_errstr(rkm));
                        rd_kafka_message_destroy(rkm);

                        if(this->GetMaxTry() > 0 && this->GetMaxTry() > iTry++) break;
                        continue;
                }

                /* Print the message key. */
                TString str = "";
                if (rkm->key)
                        str += TString::Format("[Length: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->key_len, this->groupid.Data(), rkm->offset);
                if (rkm->key && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->key, rkm->key_len))
                        str += TString::Format("Key: \"%.*s\"", (int)rkm->key_len, (const char *)rkm->key);

                /* Print the message value/payload. */
                if (rkm->payload) this->transactions++;
                if (rkm->payload)
                        str += TString::Format("[Payload: %d bytes (group \"%s\"; offset %" PRId64 ")]", (int)rkm->len, this->groupid.Data(), rkm->offset);

                /* Processing message. */
                TPrint::Debug(10, __METHOD_NAME__, "Message received in topic \"%s\" on partition: %" PRId32, rd_kafka_topic_name(rkm->rkt), rkm->partition, str.Data());
                
                TKafka::Record record;
                               record.offset = rkm->offset;
                               record.partition = rkm->partition;
                               record.topic = rd_kafka_topic_name(rkm->rkt);
                               record.epoch = time(0);

                if(rkm->key_len > 0) record.key = TString((const char *)rkm->key, rkm->key_len);
                if(rkm->len     > 0) record.msg = TString((const char *)rkm->payload, rkm->len);

                records.push_back(record);

                if(!this->output.EqualTo("")) {

                        if (rkm->payload && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->payload, rkm->len))
                                TPrint::Debug(10, __METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                
                        std::vector<unsigned char> bytes;
                                                   bytes.insert(bytes.end(), (const char *) rkm->payload,  (const char *) rkm->payload + rkm->len);

                        std::vector<TString> fIdentifiers;
                                             fIdentifiers.push_back(rd_kafka_topic_name(rkm->rkt));
                                             fIdentifiers.push_back(TString::Itoa(rkm->partition, 10));
                                             fIdentifiers.push_back(TString::Itoa(rkm->offset, 10));

                        TKafka::Save(bytes, this->output, fIdentifiers);

                } else if(verbose) {

                        if (rkm->payload && ROOT::IOPlus::Helpers::IsPrintable((const char*) rkm->payload, rkm->len))
                                TPrint::Message(__METHOD_NAME__, TString::Format("[Message received: \"%.*s\"]", (int)rkm->len, (const char *)rkm->payload));
                }

                rd_kafka_message_destroy(rkm);

                auto elapsed = std::chrono::high_resolution_clock::now() - start;
                long long milliseconds = this->delay - std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                
                this->Await(milliseconds);
        }

        return records;
}

std::vector<TKafka::Broker>  TKafka::GetBrokers() { return this->brokers; }
TKafka *TKafka::AddBroker(TKafka::Broker broker)
{
        std::vector<TKafka::Broker> _b;
                            _b.push_back(broker);

        std::vector<TKafka::Broker>::iterator it = std::search(this->brokers.begin(),this->brokers.end(),_b.begin(),_b.end(), TKafka::_match_broker);
        if(it == this->brokers.end())
                    this->brokers.push_back(broker);

        return this;
}

TKafka *TKafka::RemoveBroker(TKafka::Broker broker)
{
        std::vector<TKafka::Broker> _b;
                            _b.push_back(broker);

        std::vector<TKafka::Broker>::iterator it = std::search(this->brokers.begin(),this->brokers.end(),_b.begin(),_b.end(), TKafka::_match_broker);
        if(it != this->brokers.end())
                this->brokers.erase(it);

        return this;
}

// bool DeleteOffset(int offset)
// {
//         return 0;
// }

// bool DeleteBeforeOffset(int offset)
// {
//         return 0;
// }

ClassImp(TKafka)


