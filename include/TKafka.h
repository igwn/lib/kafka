/**
 * *********************************************
 *
 * \file TKafka.h
 * \brief Header of the TKafka class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TKafka
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TKafka_H
#define TKafka_H

#include <Riostream.h>
#include <TObject.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TString.h>

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

#include <map>
// #include <ctype.h>
#include <time.h>
#include <signal.h>
#include <string.h>

class TKafka: public TObject
{
        private:
                class Impl;
                Impl *pImpl = NULL;

        public:
                static const char *DEFAULT_HOST;
                static const int   DEFAULT_PORT;
                static const int   DEFAULT_DELAY;
                static const int   DEFAULT_TICK;

                using Dict     = std::map<TString, TString>;
                using DictIter = std::map<TString, TString>::iterator;

                typedef struct {
                        TString host;
                        int port;
                } Broker;

                typedef struct {

                        int offset;
                        int partition;
                        TString topic;

                        std::time_t epoch;

                        TString key;
                        TString msg;

                } Record;

                enum class Config {AtMostOnce, AtLeastOnce, ExactlyOnce};
                static void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
                static TString enum2str(Config config) {

                        switch(config) {

                                case Config::AtMostOnce: return "at most once";
                                case Config::AtLeastOnce: return "at least once";
                                case Config::ExactlyOnce: return "exactly once";
                                default: enum2str();
                        }
                }

                enum class Mode {Producer, Consumer, Dry};
                static TString enum2str(Mode mode) {

                        switch(mode) {

                                case Mode::Dry: return "dry-run";
                                case Mode::Producer: return "producer";
                                case Mode::Consumer: return "consumer";
                                default: enum2str();
                        }
                }

        protected:
        
                std::vector<Broker> brokers;
                static bool _match_broker(Broker b1, Broker b2) { return b1.host.EqualTo(b2.host) && b1.port == b2.port; }

                std::vector<TString> topics;
                TString groupid;

                time_t oot;

                static bool HasPattern(TString fqfn);
                static TString SubstituteIdentifier(TString pattern, TString identifier);
                static TString SubstituteIdentifier(TString pattern, std::vector<TString> identifier);

        public:

        static const int DefaultPartition;

        TKafka(TString server, int port = 9092, TString topics = "default",  TString groupid = "");
        TKafka(std::vector<TString> server,     std::vector<TString> topics, TString groupid = "");
        TKafka(std::vector<Broker> brokers,     std::vector<TString> topics, TString groupid = "");

        ~TKafka();

        TString __toString();

        void Print(Option_t *option="");
        inline bool IsPrintable(const char *str, size_t length)
        {
                for(int i = 0; i < (int) length; i++)
                        if(!isprint(str[i])) return false;

                return true;
        }

        static const char kSeparator = '-';
        static volatile sig_atomic_t bCallback;
        static volatile sig_atomic_t bInterrupt;
                volatile sig_atomic_t bRun;

        bool IsConnected() { return bRun && !bInterrupt; }
        bool Connect(Mode, Dict = {});
        int Disconnect();

        time_t GetOriginOfTime() { return oot; }
        static void EnableSignalHandler();
        static void DisableSignalHandler();
        static void HandlerCTRL_C(int);

        int GetNSubscribers();
        std::vector<Broker>  GetBrokers();
        
        TKafka *AddBroker(Broker broker);
        TKafka *RemoveBroker(Broker broker);

        Mode mode;
        Mode GetMode() { return this->mode; }
        TKafka *SetMode(Mode mode)
        {
                this->mode = mode;
                return this;
        }

        Config config;
        Config GetConfig() { return this->config; }
        TKafka *SetConfig(Config config)
        {
                this->config = config;
                return this;
        }

        std::vector<unsigned char> ReadFile(const char* filename);

        int transactions;
        int GetNTransactions() { return this->transactions; }
        void ResetTransactions() 
        { 
                this->transactions = 0;
                this->records.clear();
        }

        int max_transactions;
        int GetMaxTransactions() { return this->max_transactions; }
        TKafka *SetMaxTransactions(int max_transactions)
        {
                this->max_transactions = max_transactions;
                return this;
        }

        int max_try;
        int GetMaxTry() { return this->max_try; }
        TKafka *SetMaxTry(int max_try)
        {
                this->max_try = max_try;
                return this;
        }

        std::vector<Record> records;
        virtual void Process(Record record) { UNUSED(record); }

        std::vector<Record> Consume(double timeout = 1, bool verbose = false);
        std::vector<Record> ConsumeN(int N = 0, bool bProgressBar = false, bool verbose = false);

        bool Produce(TString topic, std::vector<unsigned char>);
        inline bool Produce(TString topic, TString message) { return TKafka::Produce(topic, message.Data()); }
        inline bool Produce(TString topic, const char *message = "")
        {
                return this->Produce(topic, str2vec(message));
        }

        inline std::vector<unsigned char> str2vec(TString message) { return str2vec(message.Data()); }
        inline std::vector<unsigned char> str2vec(const char* message) {

                std::string input(message);
                std::vector<unsigned char> output(input.length());
                std::transform(input.begin(), input.end(), output.begin(), [](char c) { return static_cast<unsigned char>(c); });

                return output;
        }

        bool Save(std::vector<unsigned char>, TString, std::vector<TString>);

        int partition;
        inline void SetPartition(int partition = /*RD_KAFKA_PARTITION_UA*/ -1) { this->partition = partition; }

        int offset;
        inline void SetOffset(int offset) { this->offset = offset; }

        time_t offsetForTimes;
        inline void SetOffsetForTimes(TString offsetForTimes, bool isDST = false, const std::string& format = "%Y-%b-%d %H:%M:%S") { this->offsetForTimes = TKafka::Time(offsetForTimes.Data(), isDST, format); }
        inline void SetOffsetForTimes(int offsetForTimes) { this->offsetForTimes = offsetForTimes; }
        static std::time_t Time(const std::string& str, bool is_dst = false, const std::string& format = "%Y-%b-%d %H:%M:%S");

        bool bOffsetLatest;
        inline void SetLatestOffset(bool bOffsetLatest) { this->bOffsetLatest = bOffsetLatest; }

        bool bLowLatency;
        inline void SetLowLatency(bool bLowLatency) { this->bLowLatency = bLowLatency; }

        bool bOffsetEarliest;
        inline void SetEarliestOffset(bool bOffsetEarliest) { this->bOffsetEarliest = bOffsetEarliest; }

        int offsetShiftBy;
        inline void ShiftOffsetBy(int offsetShiftBy) { this->offsetShiftBy = offsetShiftBy; }

        int delay;
        inline void SetDelay(int delay) { this->delay = delay < 1 ? DEFAULT_DELAY : delay; }

        int tick;
        inline void SetTick(int tick) { this->tick = tick < 1 ? DEFAULT_TICK : tick; }

        TString output;
        inline void SetOutput(const char *output) { this->output = TString(output); }

        bool bRepeat;
        inline void SetRepeat(bool bRepeat) { this->bRepeat = bRepeat; }

        void Await(long long ms);

        // Admin client to be implemented if needed
        // bool DeleteOffset(int offset);
        // bool DeleteBeforeOffset(int offset);

        // SSL Implementation

        ClassDef(TKafka,1);
};

#endif
